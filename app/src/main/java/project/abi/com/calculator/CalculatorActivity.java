package project.abi.com.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

public class CalculatorActivity extends AppCompatActivity {

    Button satuButton;
    Button duaButton;
    Button tigaButton;
    Button empatButton;
    Button limaButton;
    Button enamButton;
    Button tujuhButton;
    Button delapanButton;
    Button sembilanButton;
    Button komaButton;
    Button nolButton;
    Button kaliButton;
    Button bagiButton;
    Button tambahButton;
    Button kurangButton;
    Button samaDenganButton;
    Button clearButton;
    Button deleteButton;
    TextView hasilTV;
    private String operator = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        satuButton = (Button) findViewById(R.id.satuButton);
        duaButton = (Button) findViewById(R.id.duaButton);
        tigaButton = (Button) findViewById(R.id.tigaButton);
        empatButton = (Button) findViewById(R.id.empatButton);
        limaButton = (Button) findViewById(R.id.limaButton);
        enamButton = (Button) findViewById(R.id.enamButton);
        tujuhButton = (Button) findViewById(R.id.tujuhButton);
        delapanButton = (Button) findViewById(R.id.delapanButton);
        sembilanButton = (Button) findViewById(R.id.sembilanButton);
        komaButton = (Button) findViewById(R.id.komaButton);
        nolButton = (Button) findViewById(R.id.nolButton);
        kaliButton = (Button) findViewById(R.id.kaliButton);
        bagiButton = (Button) findViewById(R.id.bagiButton);
        tambahButton = (Button) findViewById(R.id.tambahButton);
        kurangButton = (Button) findViewById(R.id.kurangButton);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        samaDenganButton = (Button) findViewById(R.id.samaDenganButton);
        clearButton = (Button) findViewById(R.id.clearButton);
        hasilTV = (TextView) findViewById(R.id.hasilTV);
                                                                                                                                                                                                                                                                                                                                 satuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String inputValue = hasilTV.getText().toString();
               hasilTV.setText(inputValue+1);
            }
        });

        duaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+2);
            }
        });

        tigaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+3);
            }
        });

        empatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+4);
            }
        });

        limaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+5);
            }
        });

        enamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+6);
            }
        });

        tujuhButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+7);
            }
        });

        delapanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+8);
            }
        });

        sembilanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+9);
            }
        });

        nolButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                inputValue = checkNolFirstInput(inputValue);
                hasilTV.setText(inputValue+0);
            }
        });

        komaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                if(inputValue.lastIndexOf(".")!=inputValue.length()-1){
                    hasilTV.setText(inputValue+".");
                }
            }
        });

        kaliButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String inputValue = hasilTV.getText().toString();

                if(!inputValue.endsWith(".")) {

                    if(inputValue.endsWith("/") || inputValue.endsWith("*") || inputValue.endsWith("/") || inputValue.endsWith("+") || inputValue.endsWith("-")){
                        inputValue = inputValue.substring(0,inputValue.length()-1)+"*";
                        hasilTV.setText(inputValue);
                    }else{
                        hasilTV.setText(inputValue+"*");
                    }
                }

                operator ="*";
            }
        });


        bagiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                if(!inputValue.endsWith(".")) {

                    if(inputValue.endsWith("/") || inputValue.endsWith("*") || inputValue.endsWith("/") || inputValue.endsWith("+") || inputValue.endsWith("-")){
                        inputValue = inputValue.substring(0,inputValue.length()-1)+"/";
                        hasilTV.setText(inputValue);
                    }else{
                        hasilTV.setText(inputValue+"/");
                    }
                }

                operator ="/";
            }
        });

        tambahButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                if(!inputValue.endsWith(".")) {

                    if(inputValue.endsWith("/") || inputValue.endsWith("*") || inputValue.endsWith("/") || inputValue.endsWith("+") || inputValue.endsWith("-")){
                        inputValue = inputValue.substring(0,inputValue.length()-1)+"+";
                        hasilTV.setText(inputValue);
                    }else{
                        hasilTV.setText(inputValue+"+");
                    }
                }

                operator ="\\+";
            }
        });

        kurangButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                if(!inputValue.endsWith(".")) {

                    if(inputValue.endsWith("/") || inputValue.endsWith("*") || inputValue.endsWith("/") || inputValue.endsWith("+") || inputValue.endsWith("-")){
                        inputValue = inputValue.substring(0,inputValue.length()-1)+"-";
                        hasilTV.setText(inputValue);
                    }else{
                        hasilTV.setText(inputValue+"-");
                    }
                }

                operator ="-";
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                if(inputValue.length()>0){
                    String valueAfterDelete = inputValue.substring(0, inputValue.length()-1);
                    if(valueAfterDelete.equals("")){
                        valueAfterDelete = "0";
                    }
                    hasilTV.setText(valueAfterDelete);
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                hasilTV.setText("0");
                operator = null;
            }
        });

        samaDenganButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputValue = hasilTV.getText().toString();
                String[] dataList = inputValue.split("(?<=[-+*/])|(?=[-+*/])");;

                double result = Double.parseDouble(dataList[0]);

                for (int i = 1; i < dataList.length; i += 2) {
                    String op = dataList[i];
                    double val = Double.parseDouble(dataList[i+1]);
                    switch (op) {
                        case "*" :
                            result *= val;
                            break;
                        case "/" :
                            result /= val;
                            break;
                        case "+" :
                            result += val;
                            break;
                        case "-" :
                            result -= val;
                            break;
                    }
                }

                DecimalFormat decimalFormat = new DecimalFormat("0.####");

                hasilTV.setText(String.valueOf(decimalFormat.format(result)));

            }
        });


    }

    public String checkNolFirstInput(String inputValue){


        if(inputValue.startsWith("0") && inputValue.length()==1){
            inputValue = "";
        }

        return inputValue;

    }
}
